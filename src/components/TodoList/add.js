import React , {useState} from "react";
import PropTypes from 'prop-types';

AddInputTodo.propTypes = {
  submitForm:PropTypes.func
};

AddInputTodo.defaultProps = {
  submitForm:null
};

	function AddInputTodo(props) {

    const [title , setTitle] = useState('');

    function OnChangeTitle(e) {
      setTitle(e.target.value);
    }

    function submitFormHandle(e) {
      e.preventDefault();
      const {submitForm} = props;
      let valueForm =  {
        title:title,
        content:'test'
      }


      if (submitForm) {

        submitForm(valueForm);
        setTitle('');

        return;
      }
      return false;

    }

		return (
			<form onSubmit={submitFormHandle}>
        <input type="text" value={title} onChange={OnChangeTitle}/>
      </form>
		);
  }


export default AddInputTodo;
