import React from "react";
import PropTypes from 'prop-types';

TodoList.propTypes = {
  todos: PropTypes.array,
  removeTodo:PropTypes.func
};

TodoList.defaultProps = {
  todos: [],
  removeTodo:null
};


function TodoList(props) {
  const {todos , removeTodo} = props;

	return (
		<div className="todo">
      <ul>
        {
          todos ?
          todos.map((item, i)=>{
            return (
              <li key={item.id}>
                {item.title}
                <button onClick={ ()=> removeTodo(item.id) }> remove</button>
              </li>
            )
          }) : null
        }

      </ul>
    </div>
	);
}


export default TodoList;
