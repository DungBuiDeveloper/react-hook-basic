import React , {useState} from 'react';
import './color.scss'

function getColorRamdom() {
  let colors = ["red", "green", "pink", "black"];
  return colors[Math.floor(Math.random() * colors.length)];
}

function ColorBox() {

  const [color,setColor] = useState( () =>  {
    let initialColor = localStorage.getItem('box_color') || 'pink';
    return initialColor;
  });


  //Set Color
  function setColorClick() {
    let colorRandom = getColorRamdom();
    setColor(colorRandom);
    localStorage.setItem('box_color' , colorRandom);
  }

  return (
    <div>
      <div className="box" style={{ backgroundColor: color}}></div>
      <button
        onClick={ () =>  setColorClick() }
      >Click</button>

    </div>

  );
}

export default ColorBox;
