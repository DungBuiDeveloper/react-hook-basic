import React  , {useState} from 'react';
import './App.scss';
import TodoList from './components/TodoList';
import AddForm from './components/TodoList/add';

function App() {

  const [todoList , setTodoList] = useState( () =>  {
    return [
      {id:1,title:'dung 1' , content:'dung 2'},
      {id:2,title:'dung 2' , content:'dung 3'},
      {id:3,title:'dung 3' , content:'dung 3'},
    ]
  })
  //Remove Item
  function removeTodo(id) {
    let cloneTodo = [...todoList];
    let indexRemove = cloneTodo.findIndex(x => x.id === id);
    if (indexRemove < 0 ) return false;
    cloneTodo.splice(indexRemove , 1);
    setTodoList(cloneTodo);
  }

  function add(data) {
    let cloneTodo = [...todoList];
    data.id = cloneTodo.length + 1;
    cloneTodo.push(data);
    setTodoList(cloneTodo);

  }

  return (
    <div className="app">
      <h1>React Hook TodoList</h1>
      <AddForm submitForm={add}></AddForm>
      <TodoList
        todos={todoList}
        removeTodo={removeTodo}
      />

    </div>
  );
}

export default App;
